#Najpierw uruchamiamy generator (python3 generator_one.py (mozemy zmienic wczesniej jego ustawienia))
import csv
with open("bufor1.txt", "r") as file:#otwieramy plik tekstowy z danymi od genneratora
    with open("statsRR.csv", "w") as stats:
        write=csv.writer(stats)
        tick=0
        flag=False#flaga kontrolna
        quantum=5#kwant czasu (mozna zmienic)
        
        top_comp=0
        temp2=0#opisana nizej(line 34)
        wait_sum=0#suma czekania
        sorting=[]#tworzymy tabele z kolejka
        durations=[]
        def end(sorting):#sprawdzamy czy na pozycji duration sa same zera i zwracamy flage
            check=0
            for i in sorting:
                if (i[1])!=0:
            
                    return False
                    break
            return True
        for x in file:#sortowanie
            arrival_time,duration=map(int,x.split(","))#sortowanie, mapowanie po przecinku
            sorting.append([arrival_time,duration])#sortowanie
        sorting.sort(key=lambda x: x[0])#sortowanie, dajemy za pomoca funkcji lambda tablice zawierajaca arrival time i duration do tablicy sorting, sortowanie ze wgledu na arrival time
        for x in sorting:#zapisujemy tu dlugosc procesow, beda potrzebne do policzenia sredniego czasu oczekiwania
            durations.append(x[1])

        while (True):
            if end(sorting):#sprawdzamy czy juz wszystko policzone i konczymy
                break
            for x in sorting:
                temp2=sorting.index(x)#odczytujemy index danej tablicy w tablicy sorting
                arrival_time=x[0]
                duration=x[1]
                if duration==0:#jesli proces zostal juz policzony to przechodzimy do nastepnego w  kolejce
                    continue
                if tick<arrival_time:#przyjmujemy ze liczymy od najszybszego ticku, tzn zalozmy ze proces 1 przybyl w 4 ticku, to zamiast 4 iteracji zaczynamy id 4
                    tick+=1
                    break
                temp=durations[temp2]
                for i in range (quantum):# czas procesora na jeden proces (quantum)
                    print (str(sorting)+"  tick: "+str(tick))#ANALIZUJEMY KAZDY TICK
                    duration-=1
                    if duration==0:#jezeli process zostal juz policzony to wyswietlamy czas jego czekania i liczenia
                        tick+=1
                        print("Process "+str(temp2+1)+" was computed (computing+waitinig) in "+str(tick)+" ticks and have been waiting for "+str(tick-temp-arrival_time)+" ticks")
                        wait_sum+=(tick-temp-arrival_time)#czas czekania to tick zakonczenia-poczatkowa dlugosc-arrival time
                        print (str(tick-temp-arrival_time)+" czas oczekiwania")
                        write.writerow([tick, tick-temp])
                        sorting[temp2][1]=duration#zapisujemy pomniejszony czas trwania
                        
                        break
                    else:
                        tick+=1
                        sorting[temp2][1]=duration
                        continue

        print ("Everything was computed in "+ str(tick)+" ticks.")
        print ("Average waiting time: "+str((wait_sum)/len(sorting)))
