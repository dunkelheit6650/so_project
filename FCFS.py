#Najpierw uruchamiamy generator (python3 generator_one.py (mozemy zmienic wczesniej jego ustawienia))
import csv
with open("bufor1.txt", "r") as file:#otwieramy plik tekstowy z danymi od genneratora
    with open("statsFCFS.csv", "w") as stats:
        write=csv.writer(stats)
        tick=0
        process=0
        top_comp=0
        wait_sum=0
        sorting=[]
        for x in file:#sortowanie
            arrival_time,duration=map(int,x.split(","))#sortowanie
            sorting.append([arrival_time,duration])#sortowanie
        sorting.sort(key=lambda x: x[0])#sortowanie
        print(sorting)#DEBUG
        for x in sorting:#skoro sa juz posortowane ze wzgledu na arrival time, bierzemy po kolei
            arrival_time=x[0]
            duration=x[1]
            if tick<arrival_time:#czekamy na wlasciwy tick
                tick+=arrival_time
            temp= int(duration)#do statystyk
            top_comp+=duration
            while (True):
                if duration == 0:#jezeli proces zostal juz policzony
                    print("Process "+ str(process) + " was computed in " + str(tick) +" (computing+waiting) ticks, and have been waiting for " + str(tick-temp-arrival_time) + " ticks." +"\n" )
                    write.writerow([tick, tick-temp])#zapisujemy do csv
                    wait_sum+=(tick-temp-arrival_time)#suma czekania to obecny tick - duration(temp)- czas przybycia
                    process +=1#dodajemy jeden proces
                    break
                duration-=1#zbijamy czas trwania o 1
                tick+=1#podnosimy tick o 1
        print ("Everything was computed in "+ str(tick)+" ticks.")
        print ("Average computing time: "+str(top_comp/process))
        print ("Average waiting time: "+str(wait_sum/process))
        write.writerow([tick, top_comp/process, wait_sum/process])
