#Najpierw uruchamiamy generator (python3 generator_two.py (mozemy zmienic wczesniej jego ustawienia))
import csv
with open("bufor2.txt", "r") as file:#otwieramy plik tekstowy z danymi od genneratora
    with open("statsFIFO.csv", "w") as stats:
        write=csv.writer(stats)
        queue=[]
        ramka=[]
        zmiany=[]
        podmiany=0
        r=3#ramka(bufor) rozmiar (MOZNA ZMIENIC)
        x=0
        for i in file:#zapisujemy dane z generatora do kolejki
            temp=int(i)
            queue.append(temp)
        for i in range(len(queue)):
            temp=queue[i]
            if x==r:#tutaj zerujemy iterator indeksu w ktorym zmieniamy strone w ramce, jezeli iterator jest juz na ostatnim miejscu ramki
                x=0
            if temp not in ramka:#jezeli strony z kolejki nie ma w ramce:
                if len(ramka)<r:#jezeli ramka nie jest jeszce wypelniona to po prostu dodajemy
                    ramka.append(temp)
                    print("Krok "+ str(i+1)+": "+str(ramka)+" odwolanie do strony "+str(temp))
                    write.writerow([ramka])#zapisujemy do csv
                else:
                    ramka[x]=temp#iterujemy po wczesniej wspomnianym indeksie (x) i podmieniamy
                    x+=1
                    podmiany+=1#liczymy podmiany
                    print("Krok "+ str(i+1)+": "+str(ramka)+" odwolanie do strony "+str(temp)+" zaszla podmiana na pozycji " +str(x))

                    write.writerow([ramka])

            else:
                print("Krok "+ str(i+1)+": "+str(ramka)+" odwolanie do strony "+str(temp))#jezeli odwolujemy sie do strony ktora juz jest w ramce
                write.writerow([ramka])
                continue
        print("Ilosc podmian: "+str(podmiany))

